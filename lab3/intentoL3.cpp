#include <iostream>
#include <fstream>
#include <stdio.h>
#include "buscador.h" // incluye el archivo busacador.h
using namespace std;

int main(int argc, char * argv[]){ // crea la funcion principal aceptando variables 
    buscador buscador; // inicia la clase buscador
    for(int i = 1; i < argc; i++){ // recorre las variables otorgadas al iniciar el programa
        buscador.procesar_archivos(argv[i]); // inicia la funcion que obtendrá los datos
    }
    buscador.imprimir_datos(); // imprimirá los datos por terminal
    return 0; // cierre normal del programa
} 
