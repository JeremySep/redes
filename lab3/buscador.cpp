#include <iostream>
#include <fstream>
#include <stdio.h>
#include "buscador.h"
using namespace std;

buscador::buscador(){// define variables para obtener el total de lineas, palabras y letras
    this->sumatoriaLetras = 0;
    this->sumatoriaLineas = 0;
    this->sumatoriaPalabras =0;
}

void buscador::procesar_archivos(string File){
    this->espacio = " ";
    this->vacio = "";
    ifstream file (File); // se utiliza ifstream para procesar el archivo 
    string str;
    int cnt = 0;
    int cnt_car = 0;
    int cnt_pal = 0;
    while (getline(file, str)){ // se le entrega una linea a cada str y produce un ciclo por cada una de estas 
        cnt++; // suma una linea cada vez que se repita el ciclo while
        for (int i = 0; i < str.length(); i++){ // recorre toda la linea 
            if (str[i] != espacio[0]){ 
                cnt_car++; // en caso de que el caracter i no sea un espacio suma un caracter
                if (i < str.length() & (str[i+1] == espacio[0] | str[i+1] == vacio[0])){
                    cnt_pal++; // en caso de que el caracter i tenga un sucesor i+1 como espacio o solo sea vacio contará una palabra
                }
            }
        }
    }
    // se le asigna el valor total de lineas, palabras y letras a cada variable
    this->sumatoriaLetras = this->sumatoriaLetras + cnt_car;
    this->sumatoriaLineas = this->sumatoriaLineas + cnt;
    this->sumatoriaPalabras = this->sumatoriaPalabras + cnt_pal;
}

void buscador::imprimir_datos(){ // imprime los datos totales 
    cout << "las letras son: " << sumatoriaLetras << " las palabras son:" << sumatoriaPalabras << " las lineas son:" << sumatoriaLineas << endl;
}