/*
 * g++ -o calculos calculos.cpp -lpthread
 * para ver hebras:
 * ps -eLf
 * pstree
 * top -H -u usuario (Shift + f para cambiar vista)
 */

// se incluyen las librerias necesarias
#include <pthread.h>
#include <iostream>
#include <fstream>
using namespace std;


// funcion de puntero para los hilos
void * procesar_archivos(void *param);  
// se crean punteros en donde cuardar los datos de los hilos
int *sumlinea = NULL;
int *sumletra = NULL;
int *sumpalabra = NULL;
// inicia la funcion main donde puede recibir los datos junto con la llamada
int main(int argc, char *argv[]) {
    // se le asigna un valor y espacio en memoria a los punteros con variables iniciadas en cero
    int sumatoriaLetras = 0;
    int sumatoriaLineas = 0;
    int sumatoriaPalabras = 0;
    sumlinea = &sumatoriaLineas;
    sumpalabra = &sumatoriaPalabras;
    sumletra = &sumatoriaLetras;
    // el identificador de la hebras
    pthread_t tid;
    // conjunto de atributos de la hebra
    pthread_attr_t attr;
    // obtener los atributos predeterminados
    pthread_attr_init (&attr);
    // recorre las variables otorgadas al iniciar el programa
    for(int i = 1; i < argc; i++){
        // crear las hebras
        pthread_create (&tid, &attr, procesar_archivos, argv[i]);  
    }
    // Para esperar a que la hebras (hijas) terminen.
    pthread_join (tid,NULL);
    // imprime los resultados de los calculos de todas las hilos
    cout << "Las letras son: " << sumatoriaLetras << " Las palabras son: " << sumatoriaPalabras << " Las lineas son: " << sumatoriaLineas << endl;
    cout << endl;    
    return 0;
}

// la hebra inicia su ejecución en esta función
void * procesar_archivos(void * param){
    // se asignan los valores que se quieren evitar para el calculo de caracteres y
    string espacio = " ";
    string vacio = "";
    // se utiliza ifstream para procesar el archivo 
    ifstream file ((char*) param); 
    // se inicia una variable tipo string para almacenar el dato del nombre de archivo de texto
    string str;
    // se inician los valores para la suma de cada linea, palabra y letra que aparezca, todo iniciando en cero
    int cnt = 0;
    int cnt_car = 0;
    int cnt_pal = 0;
    // se le entrega una linea a cada str y produce un ciclo por cada una de estas 
    while (getline(file, str)){ 
        // suma una linea cada vez que se repita el ciclo while
        cnt++; 
        // recorre toda la linea 
        for (int i = 0; i < str.length(); i++){ 
            if (str[i] != espacio[0]){ 
                // en caso de que el caracter i no sea un espacio suma un caracter
                cnt_car++; 
                if (i < str.length() & (str[i+1] == espacio[0] | str[i+1] == vacio[0])){
                    // en caso de que el caracter i tenga un sucesor i+1 como espacio o solo sea vacio contará una palabra
                    cnt_pal++; 
                }
            }
        }
    }
    // se van sumando los valores a los punteros para lineas, palabras y letras
    *sumlinea = (int)*sumlinea + cnt;
    *sumpalabra = (int)*sumpalabra + cnt_pal;
    *sumletra = (int)*sumletra + cnt_car;
    // termina la hebra 
    pthread_exit (0);
}