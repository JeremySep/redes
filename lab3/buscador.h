#ifndef YOUTUBE_H // prevent multiple inclusions of header file
#define YOUTUBE_H
#include <iostream> // incluye libreria para imprimir datos 
#include <fstream> // incluye funciones de manejo de string
#include <stdio.h> 
using namespace std;

class buscador{ // crea clase buscador
    private: // define variables privadas para la clase
    string File; 
    string vacio;
    string espacio;
    int sumatoriaLetras; // define variable que almacenará la cantidad de letras de todos los archivos ingresados
    int sumatoriaPalabras; // define variable que almacenará la cantidad de palabras de todos los archivos ingresados
    int sumatoriaLineas; // define variable que almacenará la cantidad de lineas de todos los archivos ingresados
    public:// define funciones publicas para la clase 
    buscador();
    void procesar_archivos(string File);// busca en el archivo de texto cada caracter y hace el conteo de cada linea, palabra y letra
    void imprimir_datos(); // imprime los datos por terminal
};
#endif
