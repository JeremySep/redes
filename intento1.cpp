//Se recomienda usar el siguiente enlace: https://youtu.be/SHdCc4rEaIQ


#include <unistd.h>
#include <sys/wait.h>
#include <iostream>
#include <stdio.h>
#include "youtube.h"
#include "reproductor.h"
using namespace std;

int main (int argc, char * argv[]) {

  // crea el proceso hijo.
  pid_t pid;
  pid = fork();
  // valida la creación de proceso hijo.
  if (pid < 0) {
    cout << "No se pudo crear el proceso ...";
    cout << endl;
    return -1;

  } else if (pid == 0) {
    // Código del proceso hijo.
    // Ejecuta comando externo.
    Youtube Youtube;
    Youtube.setURL(argv[1]);
    Youtube.DESCARGA();

  } else {
    // Código proceso padre.
    // Padre espera por el término del proceso hijo.
    wait(NULL);
    }
  
  // crea el proceso hijo.
  pid_t pid2;
  pid2 = fork();
  if(pid2 < 0){
    cout << "No se pudo crear el proceso ...";
    cout << endl;
    return -1;
    }else if (pid2 == 0){Youtube Youtube;
    // Código del proceso hijo.
    // Ejecuta comando externo.
    Reproductor Reproductor;
    Reproductor.reproducir();
  }else{
    wait(NULL);
    cout << "--------------------------------------------"<<endl;
    cout << "Gracias por preferir nuestros servicios: ;)"<<endl;
  }
  
  return 0;
}

// youtube-dl --extract-audio --audio-format mp3 -o "sound.%(ext)s" https://youtu.be/SG90tHM1twQ
// vlc --audio-visual goom "sound.mp3"

